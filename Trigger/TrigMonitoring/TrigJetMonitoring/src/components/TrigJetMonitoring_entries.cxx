/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

#include "../TrigL1JetMonitorAlgorithm.h"
#include "../TrigL1JFexSRJetMonitorAlgorithm.h"

DECLARE_COMPONENT( TrigL1JetMonitorAlgorithm )
DECLARE_COMPONENT( TrigL1JFexSRJetMonitorAlgorithm )


