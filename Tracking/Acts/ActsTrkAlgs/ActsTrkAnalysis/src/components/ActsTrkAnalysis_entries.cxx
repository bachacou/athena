#include "src/PixelClusterAnalysis.h"
#include "src/StripClusterAnalysis.h"
#include "src/SpacePointAnalysis.h"

DECLARE_COMPONENT( ActsTrk::PixelClusterAnalysis )
DECLARE_COMPONENT( ActsTrk::StripClusterAnalysis )
DECLARE_COMPONENT( ActsTrk::SpacePointAnalysis )
